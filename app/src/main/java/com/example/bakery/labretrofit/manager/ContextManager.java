package com.example.bakery.labretrofit.manager;

import android.content.Context;

public class ContextManager {

    private static ContextManager instance;
    private static Context mContext;

    public static ContextManager getInstance() {
        if (instance == null)
            instance = new ContextManager();
        return instance;
    }


    private ContextManager() {

    }

    public void init(Context context) {
        ContextManager.mContext = context;
    }

    public Context getContext() {
        return mContext.getApplicationContext();
    }
}
