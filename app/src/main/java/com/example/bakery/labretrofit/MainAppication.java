package com.example.bakery.labretrofit;

import android.app.Application;

import com.example.bakery.labretrofit.manager.ContextManager;

public class MainAppication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ContextManager.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
