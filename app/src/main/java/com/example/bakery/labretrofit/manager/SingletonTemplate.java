package com.example.bakery.labretrofit.manager;

import android.content.Context;


public class SingletonTemplate {

    private static SingletonTemplate instance;

    public static SingletonTemplate getInstance() {
        if (instance == null)
            instance = new SingletonTemplate();
        return instance;
    }

    private Context mContext;

    private SingletonTemplate() {
        mContext = ContextManager.getInstance().getContext();
    }

}
