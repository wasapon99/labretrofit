package com.example.bakery.labretrofit.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.bakery.labretrofit.R;
import com.example.bakery.labretrofit.manager.ContextManager;
import com.example.bakery.labretrofit.manager.HttpManager;
import com.example.bakery.labretrofit.model.PhotoItemCollectionDao;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private ImageView ivImage;
    private TextView tvTitle;
    private TextView tvSubTitle;
    private ProgressBar pbProgress;
    private RelativeLayout rlData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivImage = findViewById(R.id.ivImage);
        tvTitle = findViewById(R.id.tvTitle);
        tvSubTitle = findViewById(R.id.tvSubTitle);
        pbProgress = findViewById(R.id.pbProgress);
        rlData = findViewById(R.id.rlData);

        initInstances();
    }

    private void initInstances() {
        Call<PhotoItemCollectionDao> call = HttpManager.getInstance().getService().loadPhotoList();
        call.enqueue(new Callback<PhotoItemCollectionDao>() {
            @Override
            public void onResponse(Call<PhotoItemCollectionDao> call, Response<PhotoItemCollectionDao> response) {
                if (response.isSuccessful()) {
                    pbProgress.setVisibility(View.INVISIBLE);
                    rlData.setVisibility(View.VISIBLE);
                    PhotoItemCollectionDao dao = response.body();
                    setView(dao);
                } else {
                    try {
                        pbProgress.setVisibility(View.INVISIBLE);
                        rlData.setVisibility(View.VISIBLE);
                        Toast.makeText(MainActivity.this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PhotoItemCollectionDao> call, Throwable t) {
                pbProgress.setVisibility(View.INVISIBLE);
                rlData.setVisibility(View.VISIBLE);
                Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setView(PhotoItemCollectionDao dao) {
        Glide.with(this)
                .load(dao.getData().get(0).getImageUrl())
                .apply(new RequestOptions().
                        placeholder(R.drawable.loading).
                        error(R.drawable.ic_launcher_foreground))
                .into(ivImage);

        tvTitle.setText(dao.getData().get(0).getCaption());
        tvSubTitle.setText(dao.getData().get(0).getUsername()+ "\n" +dao.getData().get(0).getCamera());
    }
}
