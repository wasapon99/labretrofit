package com.example.bakery.labretrofit.manager;

import com.example.bakery.labretrofit.model.PhotoItemCollectionDao;

import retrofit2.Call;
import retrofit2.http.POST;

public interface ApiService {

    @POST("list")
    Call<PhotoItemCollectionDao> loadPhotoList();
}
